var webpack = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: {
        'jcf_forms': './src',
        'jcf_forms.min': './src'
    },
    output: {
        filename: './js/[name].js'
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({
                    loader: [
                        'css-loader?sourceMap',
                        'sass-loader?sourceMap'
                    ]
                })
            },
            {
                test: /\.(woff2?|ttf|eot|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '/fonts/[name].[ext]?[hash]'
                }
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({
            filename: 'css/[name].css',
            sourceMap : true,
        }),
        new webpack.optimize.UglifyJsPlugin({
            include: /\.min\.js$/,
            sourceMaps: true,
            compress: {
                warnings: false
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            exclude: [
                /\.min\.css/,
            ],
            options: {
                postcss: [
                    require('autoprefixer')
                ],
                context: __dirname,
                output: {path: './'}
            }
        })
    ],
    devtool: 'eval-source-map'
}
